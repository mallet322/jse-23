package com.nlmk.shokin.model;

public class Description {

    private final String parameterName;
    private final String parameterTypeName;
    private final Boolean hasValue;

    public Description(String parameterName, String parameterTypeName, Boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                '}';
    }
}