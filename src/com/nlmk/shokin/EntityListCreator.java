package com.nlmk.shokin;

import com.nlmk.shokin.model.Description;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntityListCreator {

    public List<List<Description>> getDescriptionList(List<Object> objects) {

        List<List<Description>> result = new ArrayList<>();
        Class<?> entity = objects.get(0).getClass();

        for (Object object : objects) {
            if (object.getClass() != entity) {
                throw new IllegalArgumentException("Objects with different types!");
            }
            List<Field> subject = Arrays.asList(object.getClass().getDeclaredFields());
            List<Field> fields = new ArrayList<>(subject);
            List<Description> descriptions = new ArrayList<>();

            Class<?> clazz = object.getClass();
            if (clazz.getSuperclass() != null) {
                for (Field field : fields) {
                    field.setAccessible(true);
                    boolean hasValue = false;
                    try {
                        if (field.get(object) != null) {
                            hasValue = true;
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    Description description = new Description(field.getName(), field.getType().getTypeName(), hasValue);
                    descriptions.add(description);
                }
                result.add(descriptions);
            }
        }
        return result;
    }

}