package com.nlmk.shokin;

import com.nlmk.shokin.model.Description;
import com.nlmk.shokin.model.Person;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        EntityListCreator entitiesList = new EntityListCreator();
        List<Object> entities = new ArrayList<>();
        entities.add(new Person("James", "Halpert"));
        entities.add(new Person("Dwight", "Schrute", LocalDate.of(1985, 10, 11)));
        entities.add(new Person("Michael", "Scott", LocalDate.of(1980, 8, 18), "michael_scarn@gmail.com"));


        List<List<Description>> descriptionEntities = entitiesList.getDescriptionList(entities);
        for (List<Description> descriptionList : descriptionEntities) {
            System.out.println(descriptionList);

        }
    }
}